# spectest

## Idea

define testable specs of an interface.

the expected behavior of the interface is defined by the tests and
the resulting testsuite can be used, to make sure that the implementation
behaves according to the specs.

additionally the specs can be printed in markdown format.

## Usage

### 1. define your specs

```go
package mylibrary

import (
  "gitlab.com/golang-utils/spectest"
  "fmt"
)

type MyInterface1 interface {
  DoStuff1() string
  DoStuff2() string
  MyInterface2
}

type MyInterface2 interface {
  DoOtherStuff() string
}

func MySpecSuite(gen func() MyInterface1) *spectest.Suite {
  name := "MyLibSpecTests"

  if gen != nil {
    name += fmt.Sprintf(" for %T", gen())
  }

  st := spectest.NewSuite(name, "the testsuite for the library")
  sp1 := st.NewSpec("MyInterface1", "spec of the MyInterface1 interface")
  sp1.AddTest("DoStuff1", func(tt *testing.T) {
    impl1 := gen()
    got := impl1.DoStuff1()
    expected := "something"
    if got != expected {
      tt.Errorf("MyInterface1#DoStuff1() == %q // expected %q", got, expected)
    }
  })
  sp1.AddTest("DoStuff2", func(tt *testing.T) {
    impl1 := gen()
    got := impl1.DoStuff2()
    expected := "something2"
    if got != expected {
      tt.Errorf("MyInterface1#DoStuff2() == %q // expected %q", got, expected)
    }
  })
  

  sp2 := st.NewSpec("MyInterface2", "spec of the MyInterface2 interface")
  sp2.AddTest("DoOtherStuff", func(tt *testing.T) {
    impl1 := gen()
    got := impl1.DoOtherStuff()
    expected := "something else"
    if got != expected {
      tt.Errorf("MyInterface2#DoOtherStuff() == %q // expected %q", got, expected)
    }
  })

  sp1.AddSubSpec(sp2)
  st.AddSpec(sp1)

  return st
```
### 2. document the specs by printing it as markdown

```go
package main

import (
  "fmt"
  "os"
  "mylibrary"
)

func main() {
  mylibrary.MySpecSuite(nil).WriteTo(os.Stdout)
}

```

### 3. the specs can be used to verify that an implementation is behaving correctly

```go
package myimpl_test

import (
  "testing"
  "mylibrary"
)

func TestBehaviorAccodingToSpecs(t *testing.T) {
  mylibrary.MySpecSuite(func () mylibrary.MyInterface1 {
    return NewImplementation()
  }).Run(t)
}

```

## Documentation

see https://pkg.go.dev/gitlab.com/golang-utils/spectest
