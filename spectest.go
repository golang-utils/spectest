package spectest

import (
	"bytes"
	"fmt"
	"io"
	"testing"
)

type SpecTest struct {
	Name   string
	TestFn func(t *testing.T)
}

func (s *SpecTest) Call(t *testing.T) {
	if t != nil {
		s.TestFn(t)
	}
}

func NewSpec(context, description string) *Spec {
	return &Spec{Context: context, Description: description}
}

type Spec struct {
	Context     string
	Description string
	tests       []*SpecTest
	SubSpecs    []*Spec
	parent      *Spec

	// BeforeRun is a callback that is called before running the spec
	BeforeRun func(*Spec)

	// AfterRun is a callback that is called after running the spec
	AfterRun func(*Spec)

	// BeforeTest is a callback that is called before running each test of the spec
	BeforeTest func(*SpecTest)

	// AfterTest is a callback that is called after running each test of the spec
	AfterTest func(*SpecTest)
}

func (s *Spec) AddTest(name string, fn func(t *testing.T)) {
	s.tests = append(s.tests, &SpecTest{name, fn})
}

func (s *Spec) AddSubSpec(sp *Spec) {
	sp.parent = s
	s.SubSpecs = append(s.SubSpecs, sp)
}

func (s *Spec) meWithParents() []*Spec {
	res := []*Spec{s}
	if s.parent == nil {
		return res
	}

	res = append(res, s.parent.meWithParents()...)
	return res
}

func (s *Spec) beforeRuns() {
	all := s.meWithParents()
	// outer to inner
	for i := len(all) - 1; i >= 0; i-- {
		sp := all[i]
		if sp.BeforeRun != nil {
			sp.BeforeRun(s)
		}
	}
}

func (s *Spec) afterRuns() {
	all := s.meWithParents()
	// inner to outer
	for i := 0; i <= len(all)-1; i++ {
		sp := all[i]
		if sp.AfterRun != nil {
			sp.AfterRun(s)
		}
	}
}

func (s *Spec) beforeTestRuns(te *SpecTest) {
	all := s.meWithParents()
	// outer to inner
	for i := len(all) - 1; i >= 0; i-- {
		sp := all[i]
		if sp.BeforeTest != nil {
			sp.BeforeTest(te)
		}
	}
}

func (s *Spec) afterTestRuns(te *SpecTest) {
	all := s.meWithParents()
	// inner to outer
	for i := 0; i <= len(all)-1; i++ {
		sp := all[i]
		if sp.AfterTest != nil {
			sp.AfterTest(te)
		}
	}
}

func (s *Spec) Run(outername string, t *testing.T) (success bool) {
	s.beforeRuns()
	success = true

	for _, te := range s.tests {

		s.beforeTestRuns(te)

		str := fmt.Sprintf("%s%s/%s", outername, s.Context, te.Name)
		if !t.Run(str, te.TestFn) {
			success = false
		}

		s.afterTestRuns(te)
	}

	for _, sub := range s.SubSpecs {
		if !sub.Run(fmt.Sprintf("%s%s/", outername, s.Context), t) {
			success = false
		}
	}

	s.afterRuns()

	return success
}

func (s *Spec) WriteMarkdown(prefix string, wr io.Writer) {
	// TODO: pad by the level
	fmt.Fprintf(wr, prefix+"# %s\n\n%s\n\nTests:\n", s.Context, s.Description)
	for i, te := range s.tests {
		fmt.Fprintf(wr, "- [%v]-%s\n", i+1, te.Name)
	}

	fmt.Fprintln(wr)

	for _, sub := range s.SubSpecs {
		sub.WriteMarkdown(prefix+"#", wr)
	}
}

func NewSuite(name, description string) *Suite {
	return &Suite{Name: name, Description: description}
}

type Suite struct {
	Name        string
	Description string
	Specs       []*Spec
}

func (s *Suite) WriteTo(wr io.Writer) (int64, error) {
	var bf bytes.Buffer
	fmt.Fprintf(&bf, "\n# %s\n\n%s\n\n", s.Name, s.Description)
	for _, spec := range s.Specs {
		spec.WriteMarkdown("#", &bf)
	}
	return bf.WriteTo(wr)
}

func (s *Suite) AddSpec(sp *Spec) {
	s.Specs = append(s.Specs, sp)
}

func (s *Suite) Run(name string, t *testing.T) (success bool) {
	success = true

	for _, sub := range s.Specs {
		if !sub.Run(name, t) {
			success = false
		}
	}

	return success
}
