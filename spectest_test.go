package spectest

import (
	"bytes"
	"fmt"
	"io"
	"strings"
	"testing"
)

func mksuite1() *Suite {
	st := NewSuite("my suite", "this is just a testsuite")

	sp1 := NewSpec("Stringer", "spec of the stringer interface")

	sp1.AddTest("empty string", func(tt *testing.T) {
		var bf bytes.Buffer

		got := bf.String()
		expected := ""

		if got != expected {
			tt.Errorf("bytes.Buffer.String() == %q // expected %q", got, expected)
		}
	})

	sp1.AddTest("integer", func(tt *testing.T) {
		var bf bytes.Buffer
		bf.WriteByte(3)
		got := bf.String()
		expected := "\x03"

		if got != expected {
			tt.Errorf("3.String() == %q // expected %q", got, expected)
		}
	})
	st.AddSpec(sp1)

	sp2 := NewSpec("other spec", "here a subspec")
	sp2.AddTest("this fails not", func(tt *testing.T) {
		//tt.Fatalf("failing")
	})

	sp2surround := NewSpec("surround", "the surrounding spec")
	sp2surround.AddTest("this runs", func(tt *testing.T) {})
	sp2surround.AddSubSpec(sp2)

	st.AddSpec(sp2surround)
	return st
}

func mksuite2(wr io.Writer) *Suite {
	st := NewSuite("my suite", "this is just a testsuite")

	sp1 := NewSpec("Stringer", "spec of the stringer interface")
	sp1.BeforeRun = func(s *Spec) {
		fmt.Fprintf(wr, ">sp1: %s\n", s.Context)
	}
	sp1.AfterRun = func(s *Spec) {
		fmt.Fprintf(wr, "<sp1: %s\n", s.Context)
	}

	sp1.BeforeTest = func(t *SpecTest) {
		fmt.Fprintf(wr, "sp1 >TEST %s\n", t.Name)
	}

	sp1.AfterTest = func(t *SpecTest) {
		fmt.Fprintf(wr, "sp1 <TEST %s\n", t.Name)
	}

	sp1.AddTest("empty string", func(tt *testing.T) {
		var bf bytes.Buffer

		got := bf.String()
		expected := ""

		if got != expected {
			tt.Errorf("bytes.Buffer.String() == %q // expected %q", got, expected)
		}
	})

	sp1.AddTest("integer", func(tt *testing.T) {
		var bf bytes.Buffer
		bf.WriteByte(3)
		got := bf.String()
		expected := "\x03"

		if got != expected {
			tt.Errorf("3.String() == %q // expected %q", got, expected)
		}
	})
	st.AddSpec(sp1)

	sp2 := NewSpec("other spec", "here a subspec")

	sp2.AddTest("this fails not", func(tt *testing.T) {
		//tt.Fatalf("failing")
	})

	sp2.BeforeRun = func(s *Spec) {
		fmt.Fprintf(wr, ">sp2: %s\n", s.Context)
	}
	sp2.AfterRun = func(s *Spec) {
		fmt.Fprintf(wr, "<sp2: %s\n", s.Context)
	}

	sp2.BeforeTest = func(t *SpecTest) {
		fmt.Fprintf(wr, "sp2 >TEST %s\n", t.Name)
	}

	sp2.AfterTest = func(t *SpecTest) {
		fmt.Fprintf(wr, "sp2 <TEST %s\n", t.Name)
	}

	sp2surround := NewSpec("surround", "the surrounding spec")
	sp2surround.AddTest("this runs", func(tt *testing.T) {})

	sp2surround.BeforeRun = func(s *Spec) {
		fmt.Fprintf(wr, ">sp2surround: %s\n", s.Context)
	}
	sp2surround.AfterRun = func(s *Spec) {
		fmt.Fprintf(wr, "<sp2surround: %s\n", s.Context)
	}

	sp2surround.BeforeTest = func(t *SpecTest) {
		fmt.Fprintf(wr, "sp2surround >TEST %s\n", t.Name)
	}

	sp2surround.AfterTest = func(t *SpecTest) {
		fmt.Fprintf(wr, "sp2surround <TEST %s\n", t.Name)
	}

	sp2surround.AddSubSpec(sp2)

	st.AddSpec(sp2surround)
	return st
}

func TestRun(t *testing.T) {
	s1 := mksuite1()
	succ := s1.Run("", t)

	if !succ {
		t.Fatalf("suite %s did not run successfull", s1.Name)
	}
}

func TestRunHooks(t *testing.T) {
	var bf bytes.Buffer

	s2 := mksuite2(&bf)
	succ := s2.Run("", t)

	if !succ {
		t.Fatalf("suite %s did not run successfull", s2.Name)
	}

	got := strings.TrimSpace(bf.String())

	expected := strings.TrimSpace(`
>sp1: Stringer
sp1 >TEST empty string
sp1 <TEST empty string
sp1 >TEST integer
sp1 <TEST integer
<sp1: Stringer
>sp2surround: surround
sp2surround >TEST this runs
sp2surround <TEST this runs
>sp2surround: other spec
>sp2: other spec
sp2surround >TEST this fails not
sp2 >TEST this fails not
sp2 <TEST this fails not
sp2surround <TEST this fails not
<sp2: other spec
<sp2surround: other spec
<sp2surround: surround
	`)

	if got != expected {
		t.Errorf("got:\n%s\nexpected:\n%s\n", got, expected)
	}
}

func TestWrite(t *testing.T) {
	s1 := mksuite1()
	var bf bytes.Buffer

	s1.WriteTo(&bf)

	got := bf.String()
	expected := `
# my suite

this is just a testsuite

## Stringer

spec of the stringer interface

Tests:
- [1]-empty string
- [2]-integer

## surround

the surrounding spec

Tests:
- [1]-this runs

### other spec

here a subspec

Tests:
- [1]-this fails not

`

	if got != expected {
		t.Errorf("got:\n%s\nexpected:\n%s\n\n", got, expected)
	}
}
